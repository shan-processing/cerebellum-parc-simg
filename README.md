# Cerebellum parcellation singularity image

## Installation

1. **singularity**: check (singularity 2.6)[https://www.sylabs.io/guides/2.6/user-guide/installation.html] for more details

2. singularity build parc.simg recipe

## Usage

Check `doc.md` for more details
